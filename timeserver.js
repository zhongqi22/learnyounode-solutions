var net = require('net')

var portNumber = process.argv[2]

function createDate(date){
  var year = date.getFullYear()
  var month = date.getMonth() + 1

  if (month < 10) {
    month = "0" + month
  }

  var day = date.getDate()

  if (day < 10) {
    day = "0" + day
  }

  var hours = date.getHours()
  var minutes = date.getMinutes()

  return year + "-" + month + "-" + day + " " + hours + ":" + minutes + "\n"
}

var server = net.createServer(function(socket){
    socket.end(createDate(new Date()))
})
server.listen(portNumber)


/*
var net = require('net')

   function zeroFill (i) {
     return (i < 10 ? '0' : '') + i
   }

   function now () {
     var d = new Date()
     return d.getFullYear() + '-' +
       zeroFill(d.getMonth() + 1) + '-' +
       zeroFill(d.getDate()) + ' ' +
       zeroFill(d.getHours()) + ':' +
       zeroFill(d.getMinutes())
   }

   var server = net.createServer(function (socket) {
     socket.end(now() + '\n')
   })

   server.listen(Number(process.argv[2]))

*/
