let fs = require('fs')
let path = require('path');

// var ext = path.extname('/Users/Refsnes/demo_path.js');


var directory = process.argv[2]
var ext = process.argv[3]
var results = []
// console.log(ext);
fs.readdir(directory, function (err, data) {
  for (i in data) {
    let filepath = directory + "/" + data[i]
    // console.log(path.extname(filepath));
    if (path.extname(filepath) == "." + ext) {
      console.log(data[i]);
    }
  }
})


// correct implementation
/*
var fs = require('fs')
    var path = require('path')

    var folder = process.argv[2]
    var ext = '.' + process.argv[3]

    fs.readdir(folder, function (err, files) {
      if (err) return console.error(err)
      files.forEach(function (file) {
        if (path.extname(file) === ext) {
          console.log(file)
        }
      })
    })
*/
