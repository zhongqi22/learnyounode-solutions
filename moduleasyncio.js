module.exports = function(directory, extname, callback){
  var fs = require('fs')
  var path = require('path');

  var results = []

  fs.readdir(directory, function (err, data) {
    if (err) return callback(err)

    for (i in data) {
      if (path.extname(data[i]) == "." + extname) {
        results.push(data[i])
      }
    }
    // console.log(results);
    callback(null, results)
  })
}
