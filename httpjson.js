const http = require('http');
const url = require('url');

http.createServer(function(req, res){
  if (req.method == "GET") {
    var path = url.parse(req.url, true)
    var pathname = path.pathname
    var iso = path.query.iso
    var json
    if (pathname === "/api/parsetime"){
      json = parseTime(iso)
    }
    else if (pathname === "/api/unixtime"){
      json = unixtime(iso)
    }

    if (result) {
      res.writeHead(200, { 'Content-Type': 'application/json' })
      res.end(JSON.stringify(json));
    }
    else {
      res.writeHead(404)
      res.end()
    }

  }
}).listen(process.argv[2])

function parseTime(date){
  var date = new Date(date)
  var hours = date.getHours()
  var minutes = date.getMinutes()
  var seconds = date.getSeconds()

  return {
    "hour": hours,
    "minute": minutes,
    "second": seconds
  }
}

function unixtime(date){
  var date = new Date(date)
  return {"unixtime": date.getTime()}
}
