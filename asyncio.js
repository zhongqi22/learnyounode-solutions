var fs = require('fs')

// console.log(process.argv);

fs.readFile(process.argv[2], function(err, data) {
  if (err) {
    return console.log(err)
  }

  var str = data.toString()
  var lines = str.split('\n').length - 1

  console.log(lines);
});
