var http = require('http')
var bl = require('bl')

var results = []

function extractData(url, callback){
  http.get(url, function(response){
    response.pipe(bl(function(err, data){
      if(err) callback(err);
      callback(null, data.toString())
    }))
  })
}

extractData(process.argv[2], function(err, data) {
  if(err)console.error(err);
  results.push(data)

  extractData(process.argv[3], function(err, data){
    if(err) console.error(err);
    results.push(data)

    extractData(process.argv[4], function(err, data){
      if(err) console.error(err);
      results.push(data)

      results.forEach(function(data){
        console.log(data);
      })
    })
  })
})

/*
var http = require('http')
var bl = require('bl')
var results = []
var count = 0

function printResults () {
  for (var i = 0; i < 3; i++) {
    console.log(results[i])
  }
}

function httpGet (index) {
  http.get(process.argv[2 + index], function (response) {
    response.pipe(bl(function (err, data) {
      if (err) {
        return console.error(err)
      }

      results[index] = data.toString()
      count++

      if (count === 3) {
        printResults()
      }
    }))
  })
}

for (var i = 0; i < 3; i++) {
  httpGet(i)
}




*/
